// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"

//Pawn Component Includes
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

//Pawn Movement Includes
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "CollisionActor.h"
#include "TriggerActor.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComponent->SetupAttachment(RootComponent);
	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox"));
	HitboxComponent->SetupAttachment(MeshComponent);

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(MeshComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SetRelativeRotation(FRotator(-20.f, -180.f, -0.f));
	CameraBoom->bDoCollisionTest = false;

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();

	if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())) {
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
	
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up action bindings directly in the Pawn
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent)) {
		//Setup extra keyboard inputs
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AMyPawn::MoveEvent);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AMyPawn::InteractEvent);
	}
}

void AMyPawn::MoveEvent(const FInputActionValue& Value) {
	FVector2D MovementVector = Value.Get<FVector2D>();
	if (Controller != nullptr) {
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementVector.Y;
		NewLocation += GetActorRightVector() * MovementVector.X;
		SetActorLocation(NewLocation, true);
	}
}

void AMyPawn::InteractEvent(const FInputActionValue& Value) {
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT("Interact Triggered"));

	TArray<AActor*>OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);

	for (AActor* actor : OverlappingActors) {
		if (actor->IsA(ACollisionActor::StaticClass())) {
			actor->Destroy();
		}
		if (actor->IsA(ATriggerActor::StaticClass())) {
			ATriggerActor* tactor = Cast<ATriggerActor>(actor);
			tactor->OnTriggerDelegate.Broadcast();
		}
	}
}