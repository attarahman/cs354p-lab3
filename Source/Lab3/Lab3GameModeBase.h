// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB3_API ALab3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ALab3GameModeBase();
};
