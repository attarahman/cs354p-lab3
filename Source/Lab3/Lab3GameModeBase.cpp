// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab3GameModeBase.h"
#include "MyPawn.h"
#include "UObject/ConstructorHelpers.h"

ALab3GameModeBase::ALab3GameModeBase() {
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyMyPawn"));
    if (PlayerPawnBPClass.Class != nullptr) DefaultPawnClass = PlayerPawnBPClass.Class;
}